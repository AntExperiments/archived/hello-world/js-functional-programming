const letters = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

function rot13(str) {
  return str.split("").map(c => {
    return /[ !?.]/.test(c)
      ? c // do not change the letters, which should be ignored
      : letters[(letters.indexOf(c) + 13) % letters.length]
  }).join("")
}

rot13("SERR PBQR PNZC");