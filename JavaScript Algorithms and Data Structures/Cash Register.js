const worthTable = [
    ["PENNY",       0.01], 
    ["NICKEL",      0.05], 
    ["DIME",        0.1], 
    ["QUARTER",     0.25], 
    ["ONE",         1], 
    ["FIVE",        5], 
    ["TEN",         10], 
    ["TWENTY",      20], 
    ["ONE HUNDRED", 100]
  ]

function checkCashRegister(price, cash, cid) {
  const originalCid = [...cid];
  let change = (cash - price).toFixed(2)
  let usedCoins = [];

  do {
    // If you ran out of coins, then you don't have enough money
    if (cid.length == 0)
      return {status: "INSUFFICIENT_FUNDS", change: []}

    const paidCoin = cid.pop();
    const [coinName, coinValue] = worthTable.find(x => x[0] == paidCoin[0])

    // This coin can be used for payment (not too big)
    if (coinValue <= change) {

      // make sure to not use more coins, than available
      const maxCoins = Math.floor(change / coinValue)
      const availableCoins = paidCoin[1] / coinValue
      const count = Math.min(maxCoins, availableCoins)

      usedCoins.push([coinName, coinValue * count])
      change = (change - coinValue * count).toFixed(2);

    }
  } while (change > 0)

  // Check whether you need to give change back, or no
  // if (cash - price >= usedCoins.reduce((sum, coin) => sum + coin[1], 0)) {
  if (usedCoins.filter(x => x[1] > 0).join() == originalCid.filter(x => x[1] > 0).join())
    return { status: "CLOSED", change: originalCid }
  
  return { status: "OPEN", change: usedCoins }
}

checkCashRegister(19.5, 20, [["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.1], ["QUARTER", 4.25], ["ONE", 90], ["FIVE", 55], ["TEN", 20], ["TWENTY", 60], ["ONE HUNDRED", 100]])
checkCashRegister(19.5, 20, [["PENNY", 0.01], ["NICKEL", 0], ["DIME", 0], ["QUARTER", 0], ["ONE", 0], ["FIVE", 0], ["TEN", 0], ["TWENTY", 0], ["ONE HUNDRED", 0]])
checkCashRegister(19.5, 20, [["PENNY", 0.5], ["NICKEL", 0], ["DIME", 0], ["QUARTER", 0], ["ONE", 0], ["FIVE", 0], ["TEN", 0], ["TWENTY", 0], ["ONE HUNDRED", 0]]) 