function alphabeticalOrder(arr) {
  // Only change code below this line

  const result = arr.sort((a, b) =>
    a === b ? 0 : a > b ? 1 : -1
  );

  console.log(result)
  return result

  // Only change code above this line
}
alphabeticalOrder(["a", "d", "c", "a", "z", "g"]);